<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать файл в "wp-config.php"
 * и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки базы данных
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://ru.wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Параметры базы данных: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'telemehanika' );

/** Имя пользователя базы данных */
define( 'DB_USER', 'root' );

/** Пароль к базе данных */
define( 'DB_PASSWORD', 'root' );

/** Имя сервера базы данных */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8mb4' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу. Можно сгенерировать их с помощью
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}.
 *
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными.
 * Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'B+0s:7F_uSt=L?:*4MVVB3D.,z{$mq+_EJF%,s)vV3iB, Xh{iF6>X0cD8.~t#[Z' );
define( 'SECURE_AUTH_KEY',  'w%G`c&QD>$(ol5/u%FGeATg8AAi1QOOpS)1jgqT]Y!#(3a9DYe]4Hz{Xa^F_CD(X' );
define( 'LOGGED_IN_KEY',    'AMVk$l=CX{cZktcftr+a:~|l+LWe8]}}=,U462.S i}7>,b*y%e+YyF+8wdztCdj' );
define( 'NONCE_KEY',        '/{i@@%t3F<1dVfUg~1ycD1oTgyL+#[K;:luxW)n|qni>3A<Y8>YKw+dI+V@oE{8d' );
define( 'AUTH_SALT',        'yp+A_y_5c%>2q4ch97hkAT[0mzh<UeOjbQ&bcR@1BIU!2xWLdiAYCnh:9IlYiAOA' );
define( 'SECURE_AUTH_SALT', 'Ve4gRIE%xOh{FS/z-{jeh?uHxOoP<m1].y/iscAN h32u7slA5zN1esduO/)s[Oj' );
define( 'LOGGED_IN_SALT',   'm5)m%3[Z:q:cR`sL?e~c@7AMxq0PvR_sBOP9sW9m*ENNR>1eML}GD/9e-b{J`:O#' );
define( 'NONCE_SALT',       ';~e(Z/>5nuve@_!v|(xV1L52QooL`);/9 Y4e)W:/L6n_E|EcokPj0WUbw,P!tRD' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в документации.
 *
 * @link https://ru.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Произвольные значения добавляйте между этой строкой и надписью "дальше не редактируем". */



/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once ABSPATH . 'wp-settings.php';
