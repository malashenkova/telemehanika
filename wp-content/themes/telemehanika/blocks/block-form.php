
  <div class="big-form__form">
    <h2>Обратитесь к нашим специалистам</h2>
    <div class="big-form__top">
      <div class="big-form__item">
      <span>Работаем на основе проекта, ТЗ, схем и тех. требований. Если проекта нет, создадим</span>
      </div>
      <div class="big-form__item">
      <span>Подбираем комплектующие разных ценовых сегментов без навязывания</span>
      </div>
    </div>

    <?php echo do_shortcode('[contact-form-7 id="a5f2c0e"]'); ?>

    <div class="big-form__bottom">
      <div class="big-form__contact">
        <?php
        if (get_option('Телефон')) {
          $phone = get_option('Телефон');
        ?>
          <div class="big-form__contact-name">Или свяжитесь по телефону</div>
          <a href="tel:<?php echo str_replace([' ', '(', ')'], '', $phone); ?>" target="_blank" class="big-form__phone big-form__contact-val">
            <span><?php echo $phone; ?></span>
          </a>
        <?php
        }
        ?>
      </div>
      <div class="big-form__contact">
        <?php
        if (get_option('E-mail')) {
          $mail = get_option('E-mail');
        ?>
          <div class="big-form__contact-name">Почта для заявок и информации</div>
          <a href="tel:<?= $mail; ?>" target="_blank" class="big-form__mail big-form__contact-val">
            <span><?php echo $mail; ?></span>
          </a>
        <?php
        }
        ?>
      </div>
    </div>

  </div>
