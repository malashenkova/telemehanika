<div class="popup" id="lead">
  <div class="popup__inside">
    <h2>Оставить заявку</h2>
    <div class="popup__info">
      Наш менеджер свяжется с Вами в ближайшее время для уточнения информации по интересующей продукции или услуге.
    </div>
    <?php echo do_shortcode('[contact-form-7 id="da0a8b4"]'); ?>
  </div>
</div>