<footer class="page__footer">
  <div class="footer">
    <div class="container">
      <div class="footer__content">
        <div class="footer__left">
          <a href="/" class="footer__logo">


            <?php
            if (function_exists('the_custom_logo')) {
              $custom_logo_id = get_theme_mod('custom_logo');
              $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
              if (has_custom_logo()) {
                echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
              } else {
                echo '<div>' . get_bloginfo('name') . '</div>';
              }
            }
            ?>
          </a>
          <div class="footer__desc">
            <?php echo get_bloginfo('description'); ?>
          </div>
        </div>
        <div class="footer__center">

          <?php
          wp_nav_menu(
            array(
              'theme_location' => 'footer_menu',
              'container' => 'div',
              'container_class' => 'footer__menu',
              'menu_class' => 'footer-menu'
            )
          );
          ?>
        </div>
        <div class="footer__right">
          <a href="#lead" data-fancybox class="footer__btn btn">отправить заявку</a>
        </div>
      </div>
      <div class="footer__bottom">

        <div class="footer__copy">
          © 2024 ООО "ИЦ "Телемеханика". <br>Все права защищены
        </div>
        <a href="#" class="footer__policy">
          Политика конфиденциальности
        </a>

        <a href="https://dancecolor.ru/" target="_blank" class="footer__dev">
          <span>Разработано</span>
          <img src="<?= THEME_IMAGES; ?>/dev-logo.svg" alt="">
        </a>

      </div>
    </div>
  </div>
<div class="container"><?php get_template_part('blocks/block', 'popup'); ?></div>
  
</footer>
<script src="<?= THEME_LIBS; ?>/jquery-3.7.1.min.js"></script>
<script src="<?= THEME_LIBS; ?>/inputmask/jquery.inputmask.min.js"></script>
<script src="<?= THEME_LIBS; ?>/jquery-ui.min.js"></script>
<script src="<?= THEME_LIBS; ?>/swiper/swiper-bundle.min.js"></script>
<script src="<?= THEME_LIBS; ?>/select2/select2.min.js"></script>
<script src="<?= THEME_LIBS; ?>/fancybox/jquery.fancybox.min.js"></script>
<script src="<?= THEME_LIBS; ?>/validate/jquery.validate.min.js"></script>
<script src="<?= THEME_LIBS; ?>/jquery.lazy.min.js"></script>




<script src="<?= THEME_JS; ?>/script.js"></script>

<?php wp_footer(); ?>

</body>

</html>