<?php
define("THEME_DIR", get_template_directory_uri());
/*--- REMOVE GENERATOR META TAG ---*/
// remove_action('wp_head', 'wp_generator');



// ENQUEUE STYLES

function enqueue_styles()
{

    /** REGISTER css/screen.css **/
    wp_register_style('screen-style', THEME_DIR . '/style.css', array(), '1', 'all');
    wp_enqueue_style('screen-style');
}
add_action('wp_enqueue_scripts', 'enqueue_styles');

// ENQUEUE SCRIPTS




//	Constants
//	====================
define('HOME_URI', home_url());
define('THEME_URI', get_stylesheet_directory_uri());
define('THEME_IMAGES', THEME_URI . '/img');
define('THEME_CSS', THEME_URI . '/css');
define('THEME_JS', THEME_URI . '/js');
define('THEME_LIBS', THEME_URI . '/libs');
define('THEME_FONTS', THEME_URI . '/fonts');
//	Отключение wp-embed.min.js 
//	====================
remove_action('wp_head', 'wp_oembed_add_discovery_links');
remove_action('wp_head', 'wp_oembed_add_host_js');

//	Отключение канонических и коротких ссылок
//	====================
remove_action("wp_head", "rel_canonical");
remove_action('wp_head', 'wp_shortlink_wp_head');

// 	Отключение Windows Live Writer
//	====================
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'rsd_link');

//	Отключение dns-prefetch
//	====================
remove_action('wp_head', 'wp_resource_hints', 2);
//	Отключение версии WordPress со страниц, RSS, скриптов и стилей
//	====================
add_filter('the_generator', '__return_empty_string');
function rem_wp_ver_css_js($src)
{
    if (strpos($src, 'ver='))
        $src = remove_query_arg('ver', $src);
    return $src;
}
add_filter('style_loader_src', 'rem_wp_ver_css_js', 9999);
add_filter('script_loader_src', 'rem_wp_ver_css_js', 9999);

//	Отключение смайлов
//	====================
remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');


// Отключаем Гутенберг везде
add_filter('use_block_editor_for_post_type', '__return_false');

//	Удаление "Рубрика: ", "Метка: " и т.д. из заголовка архива
//	====================
add_filter('get_the_archive_title', function ($title) {
    return preg_replace('~^[^:]+: ~', '', $title);
});

//  Регистрация областей меню
//  ============================================================
add_action('after_setup_theme', function () {
    register_nav_menus(array(
        'main_menu' => 'Main menu',
        'sidebar_menu' => 'Sidebar menu',
        'footer_menu' => 'Footer menu',
    ));
});


// Добавить классы в меню

function theme_add_anchor_links_sidebar($classes, $item, $args)
{
    if ($args->theme_location == 'main_menu') {
        $classes['class'] = "wrapper_menu_a_sidebar";
    }
}

function theme_add_anchor_links($classes, $item, $args)
{
    if ($args->theme_location == 'footer_foruser') {
        $classes['class'] = "wrapper_menu_a";
    }
}

// Добавить настройки

function custom_logo_support()
{
    add_theme_support('custom-logo');
}
add_action('after_setup_theme', 'custom_logo_support');

// Отображение поля для логотипа
function display_custom_logo_field()
{
    // Получение значения текущего логотипа
    $custom_logo_id = get_theme_mod('custom_logo');
    $custom_logo_url = wp_get_attachment_image_src($custom_logo_id, 'full')[0];

    // Вывод поля для загрузки логотипа
    echo '<input type="text" name="custom_logo" id="custom_logo" value="' . $custom_logo_url . '" class="regular-text">';
    echo '<div id="logo_dropzone">Перетащите изображение сюда</div>';
}

// Добавление скрипта для обработки перетаскивания и загрузки изображения
function custom_logo_script()
{
?>
    <script>
        jQuery(document).ready(function($) {
            var dropZone = document.getElementById('logo_dropzone');

            dropZone.addEventListener('dragover', handleDragOver, false);
            dropZone.addEventListener('drop', handleFileSelect, false);

            function handleDragOver(e) {
                e.stopPropagation();
                e.preventDefault();
                e.dataTransfer.dropEffect = 'copy';
            }

            function handleFileSelect(e) {
                e.stopPropagation();
                e.preventDefault();

                var files = e.dataTransfer.files;
                var file = files[0];

                var reader = new FileReader();
                reader.onload = function(e) {
                    $('#custom_logo').val(e.target.result);
                };
                reader.readAsDataURL(file);
            }
        });
    </script>
<?php
}
add_action('customize_controls_print_footer_scripts', 'custom_logo_script');

function register_settings()
{
    $settings = ['Наименование', 'WhatsApp', 'telegram', 'Телефон', 'Адрес', 'Время', "e-mail",  "ИНН", 'ОГРН', 'БИК', 'КПП', 'Cчет']; // Здесь перечисляются все настройки, которые вы хотите добавить

    foreach ($settings as $setting) {
        register_setting('general', $setting);

        add_settings_field(
            $setting,
            ucfirst(str_replace('_', ' ', $setting)),
            'option_callback',
            'general',
            'default',
            array('label_for' => $setting)
        );
    }
}
add_action('admin_init', 'register_settings');

function option_callback($args)
{
    $option_name = $args['label_for'];
    $option_value = get_option($option_name);
    echo "<input name='{$option_name}' id='{$option_name}' type='text' value='" . esc_attr($option_value) . "' />";
}

// Contact Form 7 remove auto added p tags
add_filter('wpcf7_autop_or_not', '__return_false');


add_image_size('gallery-prev', 420, 340, true );  
?>