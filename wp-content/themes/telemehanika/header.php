<!doctype html>
<!-- paulirish.com/2008/conditional-stylesheets-vs-css-hacks-answer-neither/ -->
<!--[if lt IE 7]> <html class="no-js ie6 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" <?php language_attributes(); ?>> <![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>> <!--<![endif]-->

<head>
  <?php
  $queried_object = get_queried_object();

  if (is_a($queried_object, 'WP_Term')) {
    $taxonomy = $queried_object->taxonomy;
    $term_id = $queried_object->term_id;
  } elseif (is_a($queried_object, 'WP_Post')) {
    $taxonomy = get_post_type($queried_object);
    $term_id = $queried_object->ID;
  } else {
    $taxonomy = '';
    $term_id = '';
  }
  ?>
  <title>
    <?php if (is_front_page()) {
      echo 'Телемеханика';
    } elseif (get_field('seo-title')) {
      the_field('seo-title');
    } ?>
  </title>


  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">

  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/select2/select2.min.css" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/swiper/swiper-bundle.min.css" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/fancybox/jquery.fancybox.min.css" />
  <link rel="stylesheet" href="<?= THEME_LIBS; ?>/jquery-ui.min.css" />
  <link rel="stylesheet" href="<?= THEME_URI; ?>/style.css" />
  <!--=== WP_HEAD() ===-->
  <?php wp_head(); ?>




</head>

<body <?php body_class('body' . (is_front_page() ? ' main-page' : '')); ?>>
  <div class="page">
    <header class="page__header">
      <div class="header">
        <div class="container-big">
          <div class="header__content">
            <div class="header__left">
              <div class="logo">
                <a href="/" class="header__logo">
                  <?php
                  if (function_exists('the_custom_logo')) {
                    $custom_logo_id = get_theme_mod('custom_logo');
                    $logo = wp_get_attachment_image_src($custom_logo_id, 'full');
                    if (has_custom_logo()) {
                      echo '<img src="' . esc_url($logo[0]) . '" alt="' . get_bloginfo('name') . '">';
                    } else {
                      echo '<div>' . get_bloginfo('name') . '</div>';
                    }
                  }
                  ?>
                </a>
              </div>
            </div>
            <div class="header__center">
              <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'main_menu',
                  'container' => 'div',
                  'container_class' => 'header__menu',
                  'menu_class' => 'main-menu'
                )
              );
              ?>
            </div>
            <div class="header__right">
              <?php
              if (get_option('Телефон')) {
                $phone = get_option('Телефон');
              ?>
                <a href="tel:<?php echo str_replace([' ', '(', ')'], '', $phone); ?>" target="_blank" class="header__phone">
                <img src="<?= THEME_IMAGES; ?>/tel.svg" alt="">
                  <span><?php echo $phone; ?></span>
                </a>
              <?php
              }
              ?>
              <div class="header__socities">
                <?php
                if (get_option('WhatsApp')) {
                  $wa_link = get_option('WhatsApp');
                ?>
                  <a href="<?php echo $wa_link; ?>" target="_blank" class="header__socity">
                    <img src="<?= THEME_IMAGES; ?>/whatsapp.svg" alt="" class="header__unhov">
                    <img src="<?= THEME_IMAGES; ?>/whatsapp-color.svg" alt="" class="header__hov">
                  </a>
                <?php
                }
                ?>
                <?php
                if (get_option('Telegram')) {
                  $telegram = get_option('Telegram');
                ?>
                  <a href="<?php echo $telegram; ?>" target="_blank" class="header__socity">
                    <img src="<?= THEME_IMAGES; ?>/telegram.svg" alt="" class="header__unhov">
                    <img src="<?= THEME_IMAGES; ?>/telegram-color.svg" alt="" class="header__hov">
                  </a>
                <?php
                }
                ?>

              </div>
              <a href="#lead" data-fancybox class="header__btn btn">отправить заявку</a>
            
            <div class="header__adaptive">
              <div class="mobile-menu">
                <div class="mobile-menu__opener js-show-menu">
                  <span></span><span></span><span></span>
                </div>
                <div class="mobile-menu__inside js-menu">
                  <div class="container">
                    <?php
                    wp_nav_menu(
                      array(
                        'theme_location' => 'main_menu',
                        'container' => 'div',
                        'menu_class' => 'mobile-menu__menu'
                      )
                    );
                    ?>
                    <div class="mobile-menu__info">
                      <?php
                      if (get_option('Наименование')) {
                        $name = get_option('Наименование');
                      ?>
                        <div class="mobile-menu__contact">
                          <?= $name; ?>
                        </div>
                      <?php
                      }
                      ?>

                      <div class="mobile-menu__socities">
                        <?php
                        if (get_option('WhatsApp')) {
                          $wa_link = get_option('WhatsApp');
                        ?>
                          <a href="<?php echo $wa_link; ?>" target="_blank" class="mobile-menu__socity">
                            <img src="<?= THEME_IMAGES; ?>/whatsapp.svg" alt="">
                          </a>
                        <?php
                        }
                        ?>
                        <?php
                        if (get_option('Telegram')) {
                          $telegram = get_option('Telegram');
                        ?>
                          <a href="<?php echo $telegram; ?>" target="_blank" class="mobile-menu__socity">
                            <img src="<?= THEME_IMAGES; ?>/telegram.svg" alt="">
                          </a>
                        <?php
                        }
                        ?>

                      </div>

                      <?php
                      if (get_option('Телефон')) {
                        $phone = get_option('Телефон');
                      ?>
                        <a href="tel:<?php echo str_replace([' ', '(', ')'], '', $phone); ?>" target="_blank" class="mobile-menu__phone">
                          <?php echo $phone; ?>
                        </a>
                      <?php
                      }
                      ?>
                      <?php
                      if (get_option('e-mail')) {
                        $mail = get_option('e-mail');
                      ?>
                        <a href="mailto:<?php echo $mail; ?>" target="_blank" class="mobile-menu__mail">
                          <?php echo $mail; ?>
                        </a>
                      <?php
                      }
                      ?>

                      <?php
                      if (get_option('Адрес')) {
                        $address = get_option('Адрес');
                      ?>
                        <div class="mobile-menu__contact">
                          <?= $address; ?>
                        </div>
                      <?php
                      }
                      ?>

                      <a href="#lead" data-fancybox class="mobile-menu__btn btn">отправить заявку</a>

                    </div>
                  </div>
                </div>
              </div>
            </div>
            </div>
          </div>
    </header>