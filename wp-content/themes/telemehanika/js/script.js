$(document).ready(function () {
  if ($(this).scrollTop() > 1) {
    $(".page__header").addClass("fixed");
  } else {
    $(".page__header").removeClass("fixed");
  }
  $(window).scroll(function () {
    if ($(this).scrollTop() > 1) {
      $(".page__header").addClass("fixed");
    } else {
      $(".page__header").removeClass("fixed");
    }
  });
  $(".lazy").lazy();

  // красивый select
  if ($("select").length) {
    $("select").select2({
      minimumResultsForSearch: 5,
      minimumInputLength: 1,
      // closeOnSelect: false,
    });
  }

  if ($(".js-fake-select-active").length) {
    let text = $('.js-fake-select-list li').first().text();
    $('.js-fake-select-list li').first().addClass('active')
    $('.js-fake-select-active').text(text);
    $('.js-fake-select').val(text);
  }
  $(".js-fake-select-active").on('click', function() {
    $(this).toggleClass('open')
    $('.js-fake-select-list').toggle()
  })
  $('.js-fake-select-list li').on('click', function() {
    $('.js-fake-select-list li').removeClass('active')
    $(this).addClass('active')
    let text = $(this).text();
    $('.js-fake-select-active').text(text);
    $(".js-fake-select-active").removeClass('open')
    $('.js-fake-select-list').hide()
    $('.js-fake-select').val(text);
  })

  // маска для инаупов
  if ($("[data-inputmask]").length) {
    $(":input").inputmask();
  }
  $(".phone").on("focus", function () {
    $(this).inputmask({
      mask: "+7 (999) 999-9999",
    });
  });
  
  $(".phone").on("focusout", function () {
    $(this).inputmask("remove");
    $(this).attr('placeholder', 'Ваш телефон')
  });

  $("input").on("change", function () {
    if ($(this).val().length) {
      $(this).addClass("not-empty");
    } else {
      $(this).removeClass("not-empty");
    }
  });
  // /маска для инпупов
  $(window).resize(function () {
    $("select").select2({
      minimumResultsForSearch: 5,
    });
  });
  // drag & drop
  var $fileInput = $(".file-input");
  var $droparea = $(".file-drop-area");

  $fileInput.on("dragenter focus click", function () {
    $droparea.addClass("is-active");
  });

  $fileInput.on("dragleave blur drop", function () {
    $droparea.removeClass("is-active");
  });

  $fileInput.on("change", function () {
    var filesCount = $(this)[0].files.length;
    var $textContainer = $('.file-drop-area__title span');

    if (filesCount === 1) {
      var fileName = $(this).val().split("\\").pop();
      $textContainer.text(fileName);
    } else if (filesCount < 5) {
      $textContainer.text(filesCount + " файлa выбрано");
    }  else  {
      $textContainer.text(filesCount + " файлов выбрано");
    }
  });
  // 
  $('input, textarea').on("input", function () {
    let val = $(this).val()

  })
  // 

  $(".main-menu a[href*='#']").on("click", function (e) {
    var anchor = $(this);
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $(anchor.attr("href")).offset().top - 100,
        },
        500
      );
    e.preventDefault();
    return false;
  });

  $(".mobile-menu__menu a[href*='#']").on("click", function (e) {
    var anchor = $(this);
    $("html, body")
      .stop()
      .animate(
        {
          scrollTop: $(anchor.attr("href")).offset().top - 100,
        },
        500
      );

    $(".js-show-menu").removeClass("open");
    $("body").removeClass("open-menu");
    $(".js-menu").slideUp();
    e.preventDefault();
    return false;
  });

  $(".js-show-menu").on("click", function () {
    $(".mobile-menu").toggleClass("open");
    $("body").toggleClass("open-menu fixed");
    $(".js-menu").slideToggle();
  });

  if ($(".js-main-banner").length) {
    var swiper = new Swiper(".js-main-banner", {
      effect: "fade",
      pagination: {
        el: ".js-main-banner-pagination",
        clickable: true,
        renderBullet: function (index, className) {
          return '<span class="' + className + '">0' + (index + 1) + "</span>";
        },
      },
    });
  }

  if ($(".js-garanty-swiper").length) {
    var swiper = new Swiper(".js-garanty-swiper", {
      slidesPerView: "auto",
      loop: true,
      navigation: {
        nextEl: ".js-garanty-next",
        prevEl: ".js-garanty-prev",
      },
      breakpoints: {
        600: {
          slidesPerView: "auto",
          spaceBetween: 12,
        },
        640: {
          slidesPerView: 1,
          spaceBetween: 20,
        },
        1200: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    });
  }

  if ($(".js-services").length) {
    const slider = new Swiper(".js-services-slider", {
      direction: "vertical",
      spewd: 500,
    });
    $(".js-services-tab").on("click", function (e) {
      e.preventDefault();
      $(".js-services-tab").removeClass("active");

      let href = $(this).attr("href");
      $(".js-tab-content.active").fadeOut().removeClass("active");
      setTimeout(() => {
        $(href).fadeIn().addClass("active");
      }, 300);

      $(this).addClass("active");
      const index = $(this).index();
      slider.slideTo(index);

      $(".js-services-imgs").attr("data-circle", href);
    });
  }

  if ($(".js-gallery").length && $(window).width() < 600) {
    var swiper = new Swiper(".js-gallery", {
      loop: true,
      spaceBetween: 12,
      lazy: true,
    });
  }


  var marqueeWidth = $(".marquee p").width();
  var containerWidth = $(".marquee").width();

  if (marqueeWidth > containerWidth) {
    $(".marquee p").css("animation-duration", marqueeWidth / 100 + "s");
  }

  if ($(".js-work-swiper").length) {
    var workSwiper = new Swiper(".js-work-swiper", {
      slidesPerView: "auto",
      loop: true,
      navigation: {
        nextEl: ".js-work-next",
        prevEl: ".js-work-prev",
      },
      on: {
        init: function (event) {
          $(".js-work-swiper .swiper-slide-active").addClass("act");
        },

        slideChangeTransitionStart: function (event) {
          $(".js-work-swiper .swiper-slide").removeClass("act");
          $(".js-work-swiper .swiper-slide-active").addClass("act");
        },
      },
    });

    $(".work__slide").on("click", function (e) {
      let wrap = $('.work');
      var wrapCoordinate = wrap.offset().left + wrap.outerWidth();
      var rightCoordinate = $(this).offset().left + $(this).outerWidth();
      if (wrapCoordinate < rightCoordinate && !workSwiper.isEnd) {
        workSwiper.slideNext();
      }
    
      e.preventDefault();
      $(".js-work-swiper .swiper-slide").removeClass("act");
      $(this).addClass("act");
    });
  }

  if ($(".js-brands").length) {
    var mySwiper = new Swiper(".js-brands", {
      speed: 5000,
      loop: true,
      autoplay: {
        delay: 0,
        disableOnInteraction: false, 
      },
      cssEase: "linear",
      breakpoints: {
        0: {
          slidesPerView: 2,
        },
        450: {
          slidesPerView: 3,
        },
        600: {
          slidesPerView: 4,
        },
        980: {
          slidesPerView: 5,
        },
        1024: {
          slidesPerView: 6,
        },
        1200: {
          slidesPerView: 6,
        },
        1600: {
          slidesPerView: 7,
        },
      },
    });
  }

  $('.js-tab').on('click', function(e) {
    e.preventDefault()
    $(this).parents('.js-tab-block').find('.js-tab.active').removeClass('active')
    $(this).addClass('active')
    let href = $(this).attr('href')
    $(this).parents('.js-tab-block').find('.js-tab-content.active').hide()
    $(href).fadeIn().addClass('active')
  })

  if ($(".js-projects-swiper").length) {
    var swiper = new Swiper(".js-projects-swiper", {
      loop: true,
      noSwipingClass: "projects__gallery",
      navigation: {
        nextEl: ".js-projects-next",
        prevEl: ".js-projects-prev",
      },
     
    });
  }
  $(".js-projects-gallery-slider").each(function() {
    var swiper = new Swiper($(this)[0], {
      breakpoints: {
        600: {
          slidesPerView: "auto",
          spaceBetween: 12,
        },
        640: {
          slidesPerView: 2,
          spaceBetween: 12,
        },
        1200: {
          slidesPerView: 2,
          spaceBetween: 20,
        },
      },
    });
  });

  if ($(".js-sertification-swiper").length && $(window).width() < 600) {
    var swiper = new Swiper(".js-sertification-swiper", {
      loop: true,
      slidesPerView: 2,
      spaceBetween: 12,
    });
  }

  $('.js-questions-ask').on('click', function() {
    if (!$(this).parents('.js-questions-item').hasClass('open')) {
      
      $('.js-questions-item.open').find('.js-questions-answer').slideUp()
      $('.js-questions-item.open').removeClass('open')
    }
    $(this).parents('.js-questions-item').toggleClass('open')
    $(this).parents('.js-questions-item').find('.js-questions-answer').slideToggle()
  })

});
